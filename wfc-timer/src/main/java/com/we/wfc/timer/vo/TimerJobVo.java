package com.we.wfc.timer.vo;

import com.we.wfc.timer.entity.TimerTrigger;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.quartz.JobDataMap;

import java.util.List;

/**
 * @Description: Timer任务定制Vo
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2020/2/6 11:55 下午
 */
@Data
public class TimerJobVo {

    @ApiModelProperty(value = "任务分组")
    private String group;

    @ApiModelProperty(value = "任务名")
    private String name;

    @ApiModelProperty(value = "任务类")
    private String clazz;

    @ApiModelProperty(value = "任务描述")
    private String description;

    @ApiModelProperty(value = "任务参数Map")
    private JobDataMap dataMap;

    @ApiModelProperty(value = "是否允许并发")
    private boolean concurrentExectionDisallowed;

    @ApiModelProperty(value = "是否持久化")
    private boolean durable;

    @ApiModelProperty(value = "是否执行后更新参数Map")
    private boolean persistJobDataAfterExecution;

    @ApiModelProperty(value = "是否为恢复运行")
    private boolean requestsRecovery;

    @ApiModelProperty(value = "触发器列表")
    private List<TimerTrigger> triggerList;
}
