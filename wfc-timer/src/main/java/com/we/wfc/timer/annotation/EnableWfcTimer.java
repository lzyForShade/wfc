package com.we.wfc.timer.annotation;

import com.we.wfc.timer.WfcTimerImportSelector;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @Description: 开启定时任务注解
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2020/2/6 11:56 下午
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Import({ WfcTimerImportSelector.class})
public @interface EnableWfcTimer {

}
