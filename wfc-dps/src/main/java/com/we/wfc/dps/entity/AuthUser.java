package com.we.wfc.dps.entity;

import com.we.wfc.dps.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @Description: Auth授权返回User
 * @Author:Liangzy (Feeling)
 * @Date:Create in 2020/5/11 3:02 下午
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class AuthUser extends BaseEntity<User> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "登录名")
    private String loginName;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "手机")
    private String mobile;

    @ApiModelProperty(value = "用户头像")
    private String photo;

    @ApiModelProperty(value = "最后登陆IP")
    private String loginIp;

    @ApiModelProperty(value = "最后登陆时间", example = "2019-12-03 00:00:00")
    private Date loginDate;

    @ApiModelProperty(value = "登录状态 : 0 正常，1 异常")
    private String loginFlag;

    @ApiModelProperty(value = "登录类型前缀")
    private String loginPrefix;
}
