package com.we.wfc.dps.annotation;

import org.springframework.context.annotation.Configuration;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 单项目Jwt过期处置开关
 * SingleJwtDispose
 *
 * @author Liangzy(Feeling)
 *
 */
@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@Target({java.lang.annotation.ElementType.TYPE})
@Documented
@Configuration
public @interface SJD {

    boolean openFlag() default false;
}
