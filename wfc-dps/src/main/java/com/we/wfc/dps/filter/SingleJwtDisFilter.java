package com.we.wfc.dps.filter;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import com.we.wfc.common.utils.ConverterUtil;
import com.we.wfc.common.utils.SpringContextUtil;
import com.we.wfc.dps.annotation.SJD;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * @Description:
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2020/5/22 4:06 下午
 */
@Slf4j
@Component
@AllArgsConstructor
@Order(SecurityProperties.DEFAULT_FILTER_ORDER - 1)
public class SingleJwtDisFilter implements Filter {

    /**
     * jwt key
     */
    @Value("${we.security.jwt_key:wfc-security-jwt}")
    private String jwtKey;

    /**
     * Determine if swagger is on
     */
    @Value(value = "${we.swagger.enabled:true}")
    private boolean swaggerEnabled;

    /**
     * redis service
     */
    @Autowired
    protected StringRedisTemplate redisTemplate;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        //获取到注解开关是否已经开启
        Map<String, Object> beans =
                SpringContextUtil.getApplicationContext().getBeansWithAnnotation(SJD.class);
        if (ConverterUtil.isNotEmpty(beans)) {
            //如果已经开启
            log.info("Turn on stand-alone Jwt mode");
            HttpServletRequest request = (HttpServletRequest) servletRequest;
            HttpServletResponse response = (HttpServletResponse) servletResponse;

            String realUrl = request.getServletPath();
            // 过滤swagger
            if (swaggerEnabled && new AntPathMatcher().match("/*/v2/api-docs", realUrl)) {
                filterChain.doFilter(request, servletResponse);
            }
            /**
             * 过滤接口是否为一般校验
             * 规则说明->接口有无权限(/g,/n)为一般校验,其他所有为非一般校验
             * */
            if (wheGenMatcher(realUrl)) {
                //有权限过滤
                if (urlMatcher(realUrl)) {
                    //判断是否为/d(下载权限)前缀
                    String rs = request.getHeader(HttpHeaders.AUTHORIZATION);
                    if (ObjectUtil.isNotNull(rs)) {
                        Claims claims = parseJWT(rs, jwtKey);
                        if (ObjectUtil.isNotNull(claims)) {
                            try {
                                //获取redis 查询token是否有效 [jti]
                                String token_key = "auth:token:" + claims.getId();
                                Object user = getRedisVal(token_key);
                                if (ObjectUtil.isNotNull(user)) {
                                    //重置失效时长 (默认1小时)
                                    try {
                                        ValueOperations operations = redisTemplate.opsForValue();
                                        operations.set(token_key, user);
                                        redisTemplate.expire(token_key, 60L * 60L, TimeUnit.SECONDS);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        returnJson(response, "998", "登录超时，请重新登录");
                                    }
                                } else {
                                    returnJson(response, "998", "登录超时，请重新登录");
                                }
                            } catch (Exception e) {
                                returnJson(response, "401", "非授权访问，无效的token");
                            }
                        } else {
                            returnJson(response, "401", "非授权访问，无效的token");
                        }
                    } else {
                        returnJson(response, "401", "非授权访问,请先获取token");
                    }
                }
            } else {
                /***
                 * 验证是否为下载权限前缀
                 * 规则说明->如果是则验证id是否一致,如果一致那么将token放置到headers
                 *         如果不一致那么,则返回非授权错误信息
                 */
                if (wheDownloads(realUrl)) {
                    String authId = Optional.ofNullable(request.getParameter("id"))
                            .map(param -> Convert.convert(List.class, param).get(0))
                            .map(Object::toString)
                            .orElse(null);
                    //拼接tokenKey
                    String token_key = "auth:token:" + authId;
                    String str = Optional.ofNullable(getRedisVal(token_key))
                            .map(Object::toString)
                            .orElse(null);
                    //如果不为空,则将token放置到请求中
                    if (ConverterUtil.isNotEmpty(str)) {
                        String access_token = JSONUtil.parseObj(str)
                                .getJSONObject("tails")
                                .getStr("access_token");
                        //向headers中放文件，记得build
                        /*ServerHttpRequest host = request
                                .mutate()
                                .header(HttpHeaders.AUTHORIZATION, access_token)
                                .build();*/
                        //将现在的request 变成 change对象
                        /*ServerWebExchange build = exchange.mutate()
                                .request(host)
                                .build();*/
                        filterChain.doFilter(servletRequest, servletResponse);
                    } else {
                        returnJson(response, "401", "非授权访问，无效的token");
                    }
                }
            }
        } else {
            log.info("Turn off stand-alone Jwt mode");
        }

        //继续执行下边的过滤器
        filterChain.doFilter(servletRequest, servletResponse);
    }

    /**
     * 返回信息
     */
    private HttpServletResponse returnJson(HttpServletResponse response, String code, String message) throws IOException {
        Dict data = Dict.create()
                .set("code", code)
                .set("msg", message);


        //清空所有的头、状态、数据缓冲Buffer
        response.reset();
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().write(JSONUtil.parse(data).toString());
        response.getWriter().flush();
        response.getWriter().close();
        return response;
    }

    /**
     * 验证是否为一般校验权限
     */
    private boolean wheGenMatcher(String real_url) {
        AntPathMatcher antPathMatcher = new AntPathMatcher();
        /** 验证添加项url */
        if (antPathMatcher.match("/*/n/**", real_url) ||
                antPathMatcher.match("/*/g/**", real_url)) {
            return true;
        }
        return false;
    }

    /**
     * 认证是否需要，验证session url
     *
     * @param real_url
     * @return
     */
    private boolean urlMatcher(String real_url) {
        AntPathMatcher antPathMatcher = new AntPathMatcher();
        /** 验证添加项url */
        if (antPathMatcher.match("/*/n/**", real_url)) {
            return false;
        }
        return true;
    }

    /**
     * 验证是否为下载权限接口
     *
     * @param real_url
     * @return
     */
    private boolean wheDownloads(String real_url) {
        AntPathMatcher antPathMatcher = new AntPathMatcher();
        /** 验证添加项url */
        if (antPathMatcher.match("/*/d/**", real_url)) {
            return true;
        }
        return false;
    }

    /**
     * 解析token
     *
     * @param jsonWebToken
     * @param base64Security
     * @return
     */
    private Claims parseJWT(String jsonWebToken, String base64Security) {
        String jwt = jsonWebToken.replace("Bearer ", "");
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(base64Security.getBytes())
                    .parseClaimsJws(jwt).getBody();
            return claims;
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * 查询redis
     *
     * @param key key
     * @return object
     */
    private Object getRedisVal(final String key) {
        Object result = null;
        try {
            ValueOperations operations = redisTemplate.opsForValue();
            result = operations.get(key);
        } catch (Exception e) {
        }
        return result;
    }

    public SingleJwtDisFilter(){

    }
}
