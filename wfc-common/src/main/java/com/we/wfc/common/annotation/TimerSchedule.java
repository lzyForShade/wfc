package com.we.wfc.common.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * @Description: 定时任务调度注解
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/11/1 9:30 上午
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Component
@Inherited
public @interface TimerSchedule {
    /**
     * 是否初始化启动
     */
    boolean bootload() default true;
}
