package com.we.wfc.common.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName ConfigureParameter
 * @Description caffeine参数
 * @Author licq
 * @Date 2020/2/28 16:42
 */
@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "we.cache")
public class CaffeineProp {

    /**
     * cache的初始容量值
     */
    private final int poolInit = 50;

    /**
     * 用来控制cache的最大缓存数量，maximumSize和 maximumWeight(最大权重)不可以同时使用
     */
    private final Long maxSize = 500L;

    /**
     * 访问之后过期时间
     */
    private final Long expireAccessTime = 10L;

    /**
     * 写入之后过期时间
     */
    private final Long expireWriteTime = 10L;

    /**
     * 创建或更新之后多久刷新
     */
    private final int refreshAfterWrite = 5;

    /**
     * we:
     *   cache: #秒
     *     initialCapacity: 50
     *     maximumSize: 500
     *     expireAfterAccess: 5
     *     refreshAfterWrite: 5
     * */

}
