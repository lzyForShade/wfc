package com.we.wfc.common.caffeine;

import lombok.AllArgsConstructor;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;


/**
 * @ClassName test
 * @Description
 * @Author licq
 * @Date 2020/2/28 20:41
 */
@Component
@AllArgsConstructor
public class CaffeineUtils {

    CacheManager caffeineCacheManager;

    private final static String DEFAULT_CACHE = "default";

    /**
     * 根据key获取value
     */
    public <T> T getValue(Object key) {
        if (key == null) {
            return null;
        }
        //根据默认cache名称获取cache
        Cache cache = caffeineCacheManager.getCache(DEFAULT_CACHE);
        if (cache != null) {
            Cache.ValueWrapper wrapper = cache.get(key);
            if (wrapper != null) {
                return (T) wrapper.get();
            }
        }

        return null;
    }

    public <T> T getValue(String cacheName, Object key) {
        if (cacheName == null || key == null) {
            return null;
        }
        Cache cache = caffeineCacheManager.getCache(cacheName);
        if (cache != null) {
            Cache.ValueWrapper wrapper = cache.get(key);
            if (wrapper != null) {
                return (T) wrapper.get();
            }
        }

        return null;
    }

    public void putValue(Object key, Object value) {
        if (key == null || value == null) {
            return;
        }
        Cache cache = caffeineCacheManager.getCache(DEFAULT_CACHE);
        if (cache != null) {
            cache.put(key, value);
        }
    }

    public void putValue(String cacheName, Object key, Object value) {
        if (cacheName == null || key == null || value == null) {
            return;
        }

        Cache cache = caffeineCacheManager.getCache(cacheName);
        if (cache != null) {
            cache.put(key, value);
        }
    }
}
