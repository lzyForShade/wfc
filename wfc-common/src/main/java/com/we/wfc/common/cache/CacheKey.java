package com.we.wfc.common.cache;


import com.we.wfc.common.utils.ConverterUtil;

/**
 * @Description: 缓存key生成
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/11/1 9:30 上午
 */
public class CacheKey {

    /**
     * 用户id
     */
    public static final String USER_ID = "USER_ID_";

    /**
     * 验证码
     */
    public static final String VC = "VC_";

    /**
     * 手机号
     */
    public static final String PN = "PN";

    /**
     * 邮箱账号
     */
    public static final String EM = "EM";

    /**
     * 短信内容
     */
    public static final String SMS = "SMS";

    /**
     * redis for access_token key
     */
    public static final String AUTH_TOKEN = "auth:token:{}";

    /**
     * app user for redis key
     */
    public static final String REDIS_KEY_USER_ID = "user:id:{}";

    /**
     * redis for dict key
     */
    public static final String REDIS_KEY_DICT_TYPE = "dict:type:{}";

    /**
     * 获取手机号的缓存key
     */
    public static String getPN(String phone) {
        if (ConverterUtil.isEmpty(phone)) {
            return null;
        }
        return PN + phone;
    }

    /**
     * 获取邮箱账号的缓存key
     */
    public static String getEm(String email) {
        if (ConverterUtil.isEmpty(email)) {
            return null;
        }
        return EM + email;
    }
}
