package com.we.wfc.common.annotation;

/**
 * 空值转换器
 *
 * @author Liangzy(Feeling)
 *
 *         2017年12月16日 下午1:07:04
 */
public interface NullExchager {
    /**
     * 将字符串转换为某类型
     *
     * @param defaultVal
     *            默认值字符串
     * @return
     */
    Object exchage(String defaultVal);
}
