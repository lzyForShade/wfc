package com.we.wfc.common.interceptor;

import com.we.wfc.common.utils.ConverterUtil;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * feign 请求统一处理token拦截器问题
 *
 * @author zhangby
 * @date 2019-04-01 17:01
 */
@Configuration
public class FeignHeaderInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate template) {
        if (!template.headers().containsKey("Authorization")) {
            //获取请求
            HttpServletRequest request = getServletRequest();
            if (ConverterUtil.isNotEmpty(request)) {
                //修正Authorization大小写不一致的问题并且获取value值
                String value = ConverterUtil.toString(request.getHeader("Authorization"), request.getHeader("authorization"));
                if (ConverterUtil.isNotEmpty(value)) {
                    template.header("Authorization", value);
                }
            }
        }
    }

    /**
     * 获取Request
     */
    public HttpServletRequest getServletRequest() {

        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (ConverterUtil.isNotEmpty(requestAttributes)) {
            return ((ServletRequestAttributes) requestAttributes).getRequest();
        }

        return null;
    }
}
