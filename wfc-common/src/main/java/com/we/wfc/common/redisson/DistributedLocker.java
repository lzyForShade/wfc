package com.we.wfc.common.redisson;

import org.redisson.api.RLock;

import java.util.concurrent.TimeUnit;

/**
 * @Description: 分布式锁接口
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2020/1/7 6:38 下午
 */
public interface DistributedLocker {
    RLock lock(String lockKey);

    RLock lock(String lockKey, int timeout);

    RLock lock(String lockKey, TimeUnit unit, int timeout);

    boolean tryLock(String lockKey, TimeUnit unit, int waitTime, int leaseTime);

    boolean isLocked(String lockKey);

    void unlock(String lockKey);

    void unlock(RLock lock);
}
