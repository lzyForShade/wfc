package com.we.wfc.common;

import cn.hutool.core.lang.Validator;
import com.we.wfc.common.bo.CmuBo;
import com.we.wfc.common.cache.CacheKey;
import com.we.wfc.common.cache.JedisDao;
import com.we.wfc.common.utils.ConverterUtil;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Description: 核验验证码业务层
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2020/3/22 11:19 上午
 */
@Component
@AllArgsConstructor
@ConditionalOnBean(JedisDao.class)
public class ValidateCodeService {

    private final JedisDao jedis;


    /**
     * 校验短信验证码是否正确(不自动清除redis)
     *
     * @param account 手机号/邮箱号
     * @param code    验证码
     * @return
     */
    public Boolean checkSMSCode(String account, String code) {
        return checkCode(account, code, false);
    }

    /**
     * 校验短信验证码是否正确
     *
     * @param account    手机号/邮箱号
     * @param code       验证码
     * @param rightClean 正确就清除
     * @return
     */
    public Boolean checkSMSCode(String account, String code, boolean rightClean) {
        return checkCode(account, code, rightClean);
    }

    /**
     * 验证账号和验证码是否匹配
     * ps:发送内容为验证码的情况下
     *
     * @param account    手机号/邮箱号
     * @param validCode  验证码
     * @param rightClean 如果正确是否清除
     */
    public Boolean checkCode(String account, String validCode, boolean rightClean) {
        boolean res = false;
        CmuBo serialize = null;
        String cacheKey = null;

        if (Validator.isMobile(account)) {
            //获取缓存Key
            cacheKey = CacheKey.getPN(account);
        } else if (Validator.isEmail(account)) {
            //判断邮箱和验证码是否一致
            cacheKey = CacheKey.getEm(account);
        }
        //获取对应的缓存对象
        serialize = jedis.getSerialize(cacheKey, CmuBo.class);

        //查询验证码是否已经过期或者不正确
        if (ConverterUtil.isNotEmpty(serialize) ||
                serialize.getExprire().compareTo(new Date()) > 0 ||
                validCode.equals(serialize.getSendMsg())) {
            res = true;
        }

        //如果需要清除
        if (rightClean) {
            //改为过期
            serialize.setExprire(new Date());
            Long expire = jedis.getExpire(cacheKey);
            jedis.setSerializeExpire(cacheKey, serialize, expire.intValue());
        }

        return res;
    }
}
