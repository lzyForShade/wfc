package com.we.wfc.common.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 值转换策略注解
 *
 * @author Liangzy(Feeling)
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Component
@Inherited
public @interface CopyPropertiesStrategy {
    /** 空值转换器(可以将非String格式的空值默认值转换成其他格式) */
    Class<? extends CopyPropertiesExchager> exchager() default CopyPropertiesExchager.class;
}
