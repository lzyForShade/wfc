package com.we.wfc.common.filter;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.core.annotation.Order;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * @Description: 跨域过滤器
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/10/31 5:55 下午
 */
@Order(SecurityProperties.DEFAULT_FILTER_ORDER - 1)
public class CORSFilter extends CorsFilter {

    private CorsConfiguration corsConfiguration;

    public CORSFilter(CorsConfigurationSource configSource, CorsConfiguration corsConfiguration) {
        super(configSource);
        this.corsConfiguration = corsConfiguration;
    }

    public CorsConfiguration getCorsConfiguration() {
        return corsConfiguration;
    }

    public void setCorsConfiguration(CorsConfiguration corsConfiguration) {
        this.corsConfiguration = corsConfiguration;
    }

}