package com.we.wfc.common.utils;

import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.StrUtil;
import com.we.wfc.common.base.BaseEnum;

import java.lang.reflect.Method;
import java.util.*;

/**
 * 枚举工具类
 *
 * @author zhangby
 * @date 11/12/19 11:59 am
 */
public class EnumUtil {

    /**
     * 初始枚举
     *
     * @param tClass
     * @param value
     * @param <T>
     * @return
     */
    public static <T extends BaseEnum> T initEnum(Class<T> tClass, String value) {
        if (StrUtil.isNotBlank(value)) {
            for (T enumObj : tClass.getEnumConstants()) {
                if (value.equals(String.valueOf(enumObj.getValue()))) {
                    return enumObj;
                }
            }
        }
        return null;
    }

    /**
     * 根据枚举的字符串获取枚举的值
     *
     * @return
     */
    public static <T extends BaseEnum> List<Map<String, Object>> getAllEnum(Class<T> clazz) {
        // 得到枚举类对象
        List<Map<String, Object>> list = new ArrayList<>();
        try {
            //获取所有枚举实例
            Enum[] enumConstants = (Enum[]) clazz.getEnumConstants();
            //根据方法名获取方法
            Method getCode = clazz.getMethod("getLabel");
            Method getMessage = clazz.getMethod("getValue");
            Map<String, Object> map = null;
            for (Enum enum1 : enumConstants) {
                map = new HashMap<String, Object>();
                //执行枚举方法获得枚举实例对应的值
                map.put("label", getCode.invoke(enum1).toString());
                map.put("value", getMessage.invoke(enum1));
                list.add(map);
            }
        } catch (Exception e) {

        }
        return list;
    }

    /**
     * 枚举转换
     *
     * @return
     */
    public static <T extends BaseEnum> Dict toMap(BaseEnum baseEnum) {
        return Dict.create()
                .set("label", baseEnum.getLabel())
                .set("value", baseEnum.getValue());
    }

    /**
     * 枚举转 map
     * @param tClass
     * @param value
     * @param <T>
     * @return
     */
    public static <T extends BaseEnum> Dict toMap(Class<T> tClass, String value) {
        return toMap(initEnum(tClass, value));
    }


}
