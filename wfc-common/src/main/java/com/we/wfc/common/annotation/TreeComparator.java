package com.we.wfc.common.annotation;

import java.lang.annotation.*;
import java.util.Comparator;

/**
 * @Description: 树比较器
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/12/30 2:25 下午
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface TreeComparator {

    @SuppressWarnings("rawtypes")
    Class<? extends Comparator> value();
}
