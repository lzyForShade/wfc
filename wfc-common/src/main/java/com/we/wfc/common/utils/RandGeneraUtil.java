package com.we.wfc.common.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.SecureRandom;
import java.util.*;

/**
 * @Description: 随机生成策略
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/11/18 5:12 下午
 */
public class RandGeneraUtil {

    private static SecureRandom random = new SecureRandom();

    /**
     * 随机生成姓名
     * randomAB()-loadNames()
     * */

    /**
     * 随机返回a和b其中一个数
     */
    public static int randomAB(int a, int b) {
        return (int) ((Math.random() * Math.abs(a - b)) + Math.min(a, b));
    }

    /**
     * 生成姓氏
     *
     * @throws IOException
     */
    public static String firstName() throws IOException {
        List<String> fistNames = loadBaiJiaXing("/name/FamilyName");
        return fistNames.get(randomAB(0, fistNames.size()));
    }

    /**
     * @return
     * @throws IOException
     * @生成名字
     */
    public static String secondName(boolean male) throws IOException {
        if (male) {
            List<String> names = loadNames("/name/Male");
            return names.get(randomAB(0, names.size()));
        } else {
            List<String> names = loadNames("/name/Lady");
            return names.get(randomAB(0, names.size()));
        }
    }

    /**
     * 读取姓氏文件，获取姓氏
     *
     * @return
     * @throws IOException
     */
    private static List<String> loadBaiJiaXing(String path) throws IOException {
        //使用类加载器来加载文件
        InputStream in = RandGeneraUtil.class.getResourceAsStream(path);
        BufferedReader br = new BufferedReader(new InputStreamReader(in, "utf-8"));
        //文件读取
        String line = null;
        //结果集合
        List<String> result = new ArrayList<>(200);
        while ((line = br.readLine()) != null) {
            line = line.trim();
            //使用空白字符分割
            String[] names = line.split("\\s+");
            result.addAll(Arrays.asList(names));
        }
        return result;
    }

    /**
     * 读取百家姓文件，获取名字
     *
     * @return
     * @throws IOException
     */
    private static List<String> loadNames(String path) throws IOException {
        InputStream in = RandGeneraUtil.class.getResourceAsStream(path);
        BufferedReader br = new BufferedReader(new InputStreamReader(in, "utf-8"));
        //文件读取
        String line = null;
        //结果集合
        List<String> result = new ArrayList<>(200);
        while ((line = br.readLine()) != null) {
            line = line.trim();
            //使用空白字符分割
            String[] names = line.split("\\s+");
            result.addAll(Arrays.asList(names));
        }
        return result;
    }

    /**
     * 随机生成ID
     * uuid()-timeStamp()
     */

    /**
     * 封装JDK自带的UUID, 通过Random数字生成, 中间无-分割.
     */
    public static String uuid() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 使用SecureRandom随机生成Long.
     */
    public static long randomLong() {
        return Math.abs(random.nextLong());
    }

    /**
     * 使用时间戳生成id
     */
    public static long timeStamp() {
        return Calendar.getInstance().getTimeInMillis();
    }

    public static void main(String[] args) {

    }
}
