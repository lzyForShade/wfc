package com.we.wfc.common.caffeine;

import com.github.benmanes.caffeine.cache.AsyncLoadingCache;
import com.github.benmanes.caffeine.cache.CacheLoader;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.we.wfc.common.properties.CaffeineProp;
import lombok.AllArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * @ClassName CaffeineCfg
 * @Description
 * @Author licq
 * @Date 2020/2/28 22:05
 */
@Configuration
@EnableCaching
@AllArgsConstructor
@EnableConfigurationProperties({CaffeineProp.class})
public class CaffeineConfig {

    private final CaffeineProp caffeineProp;

    @Bean("caffeine")
    public CacheManager cacheManager() {
        CaffeineCacheManager cacheManager = new CaffeineCacheManager();
        //构建模式初始化
        Caffeine caffeine = Caffeine.newBuilder()
                .initialCapacity(caffeineProp.getPoolInit())
                .maximumSize(caffeineProp.getMaxSize())
                .expireAfterAccess(caffeineProp.getExpireAccessTime(), TimeUnit.SECONDS)
                .expireAfterWrite(caffeineProp.getExpireWriteTime(), TimeUnit.SECONDS)
                .refreshAfterWrite(caffeineProp.getRefreshAfterWrite(), TimeUnit.SECONDS);
        cacheManager.setCaffeine(caffeine);
        //false不允许value为null
        cacheManager.setAllowNullValues(false);
        cacheManager.setCacheLoader(cacheLoader());
        return cacheManager;
    }

    /**
     * 必须要指定这个Bean，refreshAfterWrite配置属性才生效
     */
    @Bean
    public CacheLoader<Object, Object> cacheLoader() {
        return new CacheLoader<Object, Object>() {
            @Override
            public Object load(Object key) throws Exception {
                return null;
            }

            // 重写这个方法将oldValue值返回回去，进而刷新缓存
            @Override
            public Object reload(Object key, Object oldValue) throws Exception {
                return oldValue;
            }
        };
    }

    /**
     * 同步加载
     */
    public Object syncOperator(String key) {
        LoadingCache<String, Object> cache = Caffeine.newBuilder()
                .maximumSize(caffeineProp.getMaxSize())
                .expireAfterWrite(caffeineProp.getExpireWriteTime(), TimeUnit.MINUTES)
                .build(k -> setValue(key).apply(key));
        return cache.get(key);
    }

    public Function<String, Object> setValue(String key) {
        return t -> key + "value";
    }


    /**
     * 异步加载
     */
    public Object asyncOperator(String key) {
        AsyncLoadingCache<String, Object> cache = Caffeine.newBuilder()
                .maximumSize(caffeineProp.getMaxSize())
                .expireAfterWrite(caffeineProp.getExpireWriteTime(), TimeUnit.MINUTES)
                .buildAsync(k -> setAsyncValue(key).get());

        return cache.get(key);
    }

    public CompletableFuture<Object> setAsyncValue(String key) {
        return CompletableFuture.supplyAsync(() -> {
            return key + "value";
        });
    }
}
