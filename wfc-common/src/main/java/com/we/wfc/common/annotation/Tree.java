package com.we.wfc.common.annotation;

import java.lang.annotation.*;

/**
 * @Description: 树注解类
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/12/30 2:25 下午
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Tree {
    
    /** 树形ID */
    String id();
    
    /** 树形父ID */
    String pid();
}
