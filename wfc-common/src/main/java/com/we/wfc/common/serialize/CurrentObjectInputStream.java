package com.we.wfc.common.serialize;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;

/**
 * @Description: devtools会导致反序列化的classloader不正确，这里修正为获取当前线程的classloader
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/11/4 10:38 上午
 */
public class CurrentObjectInputStream extends ObjectInputStream {

    public CurrentObjectInputStream(InputStream in) throws IOException {
        super(in);
    }

    @Override
    protected Class<?> resolveClass(ObjectStreamClass desc) throws IOException, ClassNotFoundException {
        String name = desc.getName();
        return Class.forName(name, false, Thread.currentThread().getContextClassLoader());
    }
}
