package com.we.wfc.common.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * @Description: Spring加载完成后将调用的注解
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/11/1 9:30 上午
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Component
@Inherited
public @interface LoadCompleted {

    /**
     * 权重 默认0 越大越优先
     */
    int weight() default 0;
}
