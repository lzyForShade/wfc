package com.we.wfc.common.exception;

public interface WeReturn {

    String getCode();

    String getMsg();
}
