package com.we.wfc.common.serialize;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

/**
 * @Description: Gson序列化排除器
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/12/30 2:25 下午
 */
public class IgnoreStrategy implements ExclusionStrategy {
    @Override
    public boolean shouldSkipClass(Class<?> clazz) {
        return false;
    }

    @Override
    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        JsonIgnore ignoreAnn = fieldAttributes.getAnnotation(JsonIgnore.class);
        if (null == ignoreAnn) {
            return false;
        }
        return true;

    }

}
