package com.we.wfc.common.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import springfox.documentation.service.ApiInfo;

/**
 * @Description: Swagger配置文件类
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/11/18 2:25 下午
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "we.swagger")
public class SwaggerProp {

    /**
     * 是否启用
     * */
    private boolean enable = false;

    /**
     * 访问的包路径
     */
    private String apisPath;

    /**
     * 标题
     */
    private String title = ApiInfo.DEFAULT.getTitle();

    /**
     * 主机地址
     */
    private String host;

    /**
     * 访问路径
     */
    private String serviceUrl;

    /**
     * 描述
     */
    private String description = ApiInfo.DEFAULT.getDescription();

    /**
     * 开发者
     */
    private String author;

    /**
     * 邮件
     */
    private String email;

    /**
     * license
     */
    private String license = ApiInfo.DEFAULT.getLicense();

    /**
     * license地址
     */
    private String licenseUrl = ApiInfo.DEFAULT.getLicenseUrl();

    /**
     * 版本号
     */
    private String version = ApiInfo.DEFAULT.getVersion();
}
