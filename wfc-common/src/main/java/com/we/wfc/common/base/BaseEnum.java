package com.we.wfc.common.base;

import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

/**
 * 基础枚举类
 *
 * @param <T>
 * @author zhangby
 * @date 27/9/19 6:03 pm
 */
public interface BaseEnum<T extends Serializable> extends IEnum<T> {

    /**
     * 枚举数据库存储值
     */
    String getLabel();

    @Override
    T getValue();

    /**
     * 格式化枚举
     *
     * @return
     */
    @JsonValue
    default T getEnums() {
        return getValue();
    }

}