# wfc

# 入门和安装

<p align="center">
  <img src="https://portrait.gitee.com/uploads/avatars/namespace/1725/5175379_lzyForShade_1578992573.png" style="width:120px" class="no-zoom" />
</p>
<p align="center">
	<strong>A complete suite of microservices solutions.</strong>
</p>
<p align="center">
	<a target="_blank" href="https://www.apache.org/licenses/LICENSE-2.0.html">
        <img src="https://img.shields.io/:license-Apache%20License%202.0-blue.svg" />
	</a>
	<a target="_blank" href="https://www.oracle.com/technetwork/java/javase/downloads/index.html">
		<img src="https://img.shields.io/badge/JDK-8+-green.svg" />
	</a>
	<a target="_blank" href="https://spring.io/projects/spring-cloud>learn">
		<img src="https://img.shields.io/badge/Spring%20Cloud-Hoxton.SR1-gold.svg" />
	</a>
	<a target="_blank" href="https://spring.io/projects/spring-cloud>learn">
    		<img src="https://img.shields.io/badge/Spring%20Boot-2.2.2.RELEASE-red.svg" />
    </a>
</p>

> 简介

wfc致力于提供微服务解决方案,wfc架构的说明文档

> wfc名字的由来

wfc是由 "We Framework Cloud" 中的首字母组成而来，而其中的We代表这个世界上没有一件事情是靠个人能力完成的。
常言道，众人拾材火焰高,而一个团队真正需要的是团结,凝聚力,同心同德。而此架构的愿景是,除了我个体的强大以外,还因为我属于团队!
我们始终依着内心里坚固的道德原则，爱家人，爱别人。我们，在迷蒙而厚重的快乐中始终能看到对方。

> wfc如何改变我们微服务治理的方式

以前我们写业务的时候总是会引入各种各样的依赖,从而用原生的代码去一遍一遍的写。但同时又缺乏一个业务代码仓库,并且还需要组织起来。
现在只需要引入wfc即可,虽然多数业务wfc都会有涉及,但是他并不臃肿,因为所有的业务都被组件化,且可插拔、可配置。
简单、快捷、高效的实现我们的业务功能。让更多的时间花费在更多有意义的事情上。汗水不流在耕耘的路上。

> 服务组件

|组件名称| 功能名称     |
|---| -------------- |
|we-common|项目引擎|
|wfc-dps|管理组件依赖|
|wfc-security|统一认证授权中心|
|wfc-user|用户RBAC组件|
|wfc-system|系统服务组件|
|wfc-file|文件组件|
|wfc-timer|任务调度组件|
|wfc-creater|构建者组件|
|wfc-sender|发送者组件组件|
|wfc-monitor|监控组件|

> 核心技术栈
- SpringCloud全生态
   1. SpringSecurity作为安全服务
   2. SpringGateway作为路由服务
-. Spring-data-jpa作为持久层orm框架

> 链接区域
- [快速开始](http://lzyforshade.gitee.io/wfcd/#/)

- [和朋友一起维护的知识库](https://zhangbiyu.gitlab.io/basics_project/#/)
