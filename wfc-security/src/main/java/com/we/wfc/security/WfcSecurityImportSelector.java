package com.we.wfc.security;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages = "com.we.*")
@MapperScan("com.we.wfc.security.mapper")
@ComponentScan(basePackages = {"com.we.wfc.common", "com.we.wfc.security"})
public class WfcSecurityImportSelector {

}
