package com.we.wfc.security.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;

@Data
@Accessors(chain = true)
@AllArgsConstructor
public class OauthRole implements GrantedAuthority {

    private String id;
    private String name;

    @Override
    public String getAuthority() {
        return this.name;
    }
}
