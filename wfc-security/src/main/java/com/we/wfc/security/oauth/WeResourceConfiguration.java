package com.we.wfc.security.oauth;

import com.we.wfc.dps.filter.SingleJwtDisFilter;
import com.we.wfc.security.constants.GrantConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

/**
 * 资源服务器配置
 *
 * @author zhangby
 * @date 2019-06-20 15:43
 */
@Configuration
@Slf4j
@EnableResourceServer
public class WeResourceConfiguration extends ResourceServerConfigurerAdapter {

    @Autowired
    TokenStore tokenStore;

    /**
     * 配置资源的访问规则->请求之后授权之前
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                //关闭cors
                .cors().disable()
                //关闭退出登陆
                .logout().disable()
                .addFilterBefore(new SingleJwtDisFilter(), BasicAuthenticationFilter.class)
                .authorizeRequests()
                //增加非拦截器内容
                .regexMatchers(GrantConstants.FILTER_ALLOW_PATH.split(",")).permitAll()
                //增加拦截请求，除了非拦截接口，其他全部拦截
//                .regexMatchers(GrantConstants.FILTER_NOT_ALLOW_PATH.split(",")).authenticated();
                .antMatchers("/**").authenticated();
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.tokenStore(tokenStore);
    }
}
