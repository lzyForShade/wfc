package com.we.wfc.security.annotation;

import com.we.wfc.security.oauth.MVCConfig;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.lang.annotation.*;

/**
 * jwt拦截器，默认拦截验证登录用户 token
 *
 * @author zhangby
 * @date 18/1/20 1:51 pm
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({MVCConfig.class})
public @interface EnableJwtInterceptor {

}
