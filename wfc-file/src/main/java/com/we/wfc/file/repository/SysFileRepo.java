package com.we.wfc.file.repository;

import com.we.wfc.file.entity.SysFile;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * <p>
 * 系统文件表 Repository 接口
 * </p>
 *
 * @author WeCreater
 * @since 2019-12-20
 */
public interface SysFileRepo extends JpaRepository<SysFile, String> {

}
