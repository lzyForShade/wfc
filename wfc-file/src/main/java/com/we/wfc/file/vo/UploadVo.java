package com.we.wfc.file.vo;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.we.wfc.common.utils.ConverterUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

/**
 * @Description: 文件上传返回对象
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/12/10 6:17 下午
 */
@Setter
@Getter
public class UploadVo {

    @ApiModelProperty(value = "返回信息")
    private Map<String, List<FileVo>> filesMap = Maps.newHashMap();

    public void addFileVo(String paramName, FileVo fileVo) {
        List<FileVo> fileList = this.filesMap.get(paramName);
        if (ConverterUtil.isEmpty(fileList)) {
            fileList = Lists.newArrayList();
        }

        fileList.add(fileVo);
        this.filesMap.put(paramName, fileList);
    }
}
