package com.we.wfc.file;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"com.we.wfc.common", "com.we.wfc.file"})
@EnableJpaRepositories({"com.we.wfc.file.repository"})
@EntityScan({"com.we.wfc.file.entity", "com.we.wfc.security.entity"})
public class WfcFileImportSelector {

}
