package com.we.wfc.user.dto;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.we.wfc.common.utils.CommonUtil;
import com.we.wfc.dps.base.BaseDto;
import com.we.wfc.dps.entity.Role;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description:
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2020/3/16 5:54 下午
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class RoleQueryDto extends BaseDto<Role> {

    private String id;
    private String name;
    private String enname;
    private String roleType;
    private String dataScope;
    private String loginPrefix;
    private String manage;
    private String ussi;

    @Override
    public LambdaQueryWrapper<Role> queryWrapper() {
        LambdaQueryWrapper<Role> queryWrapper = new LambdaQueryWrapper<>();
        //设置查询条件
        CommonUtil.emptyStr(id).ifPresent(str -> queryWrapper.like(Role::getId, str));
        CommonUtil.emptyStr(name).ifPresent(str -> queryWrapper.like(Role::getName, str));
        CommonUtil.emptyStr(enname).ifPresent(str -> queryWrapper.like(Role::getEnname, str));
        CommonUtil.emptyStr(roleType).ifPresent(str -> queryWrapper.like(Role::getRoleType, str));
        CommonUtil.emptyStr(dataScope).ifPresent(str -> queryWrapper.like(Role::getDataScope, str));
        CommonUtil.emptyStr(loginPrefix).ifPresent(str -> queryWrapper.like(Role::getLoginPrefix, str));
        CommonUtil.emptyStr(ussi).ifPresent(str -> queryWrapper.like(Role::getUssi, str));
        CommonUtil.emptyStr(manage).ifPresent(str -> queryWrapper.eq(Role::getManage, str));
        //排序
        queryWrapper.orderByDesc(Role::getCreateDate);
        return queryWrapper;
    }
}
