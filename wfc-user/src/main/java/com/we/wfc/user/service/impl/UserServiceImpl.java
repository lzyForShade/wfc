package com.we.wfc.user.service.impl;

import cn.hutool.core.lang.Validator;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.we.wfc.common.base.BaseException;
import com.we.wfc.common.enums.ReturnCode;
import com.we.wfc.common.utils.ConverterUtil;
import com.we.wfc.dps.entity.Role;
import com.we.wfc.dps.entity.User;
import com.we.wfc.dps.entity.UserRole;
import com.we.wfc.user.dto.UserSaveDto;
import com.we.wfc.user.mapper.UserDaoU;
import com.we.wfc.user.service.IUserService;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户表  服务实现类
 * </p>
 *
 * @author zhangby
 * @since 2019-12-03
 */
@Service
@AllArgsConstructor
public class UserServiceImpl extends ServiceImpl<UserDaoU, User> implements IUserService {

    @Override
    public Function<User, User> preInit() {
        return user -> {
            List<UserRole> userRoles = new UserRole()
                    .selectList(new LambdaQueryWrapper<UserRole>().eq(UserRole::getUserId, user.getId()));
            user.set("roleIds", userRoles.stream().map(UserRole::getRoleId).collect(Collectors.toList()));
            return user;
        };
    }

    /**
     * 保存用户
     *
     * @param userSaveDto userSaveDto
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveUser(UserSaveDto userSaveDto) {
        //验证用户名是否存在
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<User>()
                .eq(User::getLoginName, userSaveDto.getLoginName());
        List<User> list = list(queryWrapper);
        if (!list.isEmpty()) {
            throw new BaseException(ReturnCode.USER_ALREADY_EXISTS);
        }
        User user = userSaveDto.convert();
        user.preInsert();
        save(user);
        // 插入角色
        for (String roleId : userSaveDto.getRoleId().split(",")) {
            new UserRole().setRoleId(roleId).setUserId(user.getId()).insert();
        }
    }

    /**
     * 更新用户
     *
     * @param id          id
     * @param userSaveDto u
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateUser(String id, UserSaveDto userSaveDto) {
        //验证用户名是否存在
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<User>()
                .eq(User::getLoginName, userSaveDto.getLoginName())
                .ne(User::getId, id);
        List<User> list = list(queryWrapper);
        if (!list.isEmpty()) {
            throw new BaseException(ReturnCode.USER_IS_NOT_EXIST);
        }
        User user = userSaveDto.convert();
        user.setId(id);
        user.preUpdate();
        updateById(user);
        //刷新角色信息
        new UserRole().delete(new LambdaQueryWrapper<UserRole>().eq(UserRole::getUserId, user.getId()));
        // 插入角色
        for (String roleId : userSaveDto.getRoleId().split(",")) {
            new UserRole().setRoleId(roleId).setUserId(user.getId()).insert();
        }
    }

    /**
     * 登录即注册用户(自动归纳角色)
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public User userRegister(String account, String lpre, String ver) {
        //参数定义
        User user = new User();
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        //判断账号类型
        if (Validator.isMobile(account)) {
            user.setMobile(account);
        } else if (Validator.isEmail(account)) {
            user.setEmail(account);
        }
        //初始化用户信息(如果是手机号,那么 mobile&loginName 字段都是手机号)
        user.setLoginName(account);

        //如果用户没有填写验证标识
        if (null == ver) {
            ver = ConverterUtil.randomMix(6);
        }
        user.setRdmPsd(ver);
        //加密成正式密码
        user.setPassword(passwordEncoder.encode(ver));
        user.preInsert();
        //添加用户
        save(user);

        //插入角色
        Role role = new Role().selectOne(new LambdaQueryWrapper<Role>().eq(Role::getLoginPrefix, lpre));
        new UserRole()
                .setRoleId(role.getId())
                .setUserId(user.getId())
                .insert();

        return user;
    }
}
