package com.we.wfc.user.controller;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.common.collect.Lists;
import com.we.wfc.common.cache.CacheKey;
import com.we.wfc.common.enums.ReturnCode;
import com.we.wfc.common.pojo.ResultPoJo;
import com.we.wfc.common.utils.CommonUtil;
import com.we.wfc.common.utils.ConverterUtil;
import com.we.wfc.dps.entity.AuthUser;
import com.we.wfc.dps.entity.Role;
import com.we.wfc.dps.entity.User;
import com.we.wfc.dps.entity.UserRole;
import com.we.wfc.dps.service.IRedisService;
import com.we.wfc.user.service.AuthFocusService;
import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

/**
 * 系统内部用户授权管理
 *
 * @author zhangby
 * @date 4/12/19 2:51 pm
 */
@RestController
@AllArgsConstructor
@Api(tags = "用户授权管理")
@RequestMapping("/n/oauth")
public class UserAuthController {

    private final IRedisService redisService;

    private final AuthFocusService authFocusService;

    /**
     * 登录验证
     *
     * @return []
     */
    @GetMapping("/login/verify")
    @ApiOperation(
            value = "登录验证", notes = "登录验证：flag【true 成功】，【false 失败】",
            produces = "application/json", response = ResultPoJo.class)
    public ResultPoJo verifyLogin(@ApiIgnore HttpServletRequest request) {
        List<String> rsList = Lists.newArrayList();
        String token = request.getHeader(HttpHeaders.AUTHORIZATION);
        //解析token
        Claims claims = CommonUtil.parseJWT(token);
        Optional.ofNullable(claims).ifPresent(cl -> {
            String tokenKey = StrUtil.format(CacheKey.AUTH_TOKEN, cl.getId());
            Object user = redisService.get(tokenKey);
            Optional.ofNullable(user).ifPresent(val -> {
                //更新登录超时时间
                redisService.set(tokenKey, user, 60L * 60L);
                rsList.addAll(Convert.toList(String.class, claims.get("authorities")));
            });
        });
        return ResultPoJo.ok(rsList);
    }

    /**
     * 用户登录
     *
     * @return
     */
    @GetMapping("/login")
    @ApiOperation(value = "登录管理", notes = "支持用户端(PC前台/APP/miniapp)用户无账号登录", produces = "application/json")
    public ResultPoJo<?> login(
            @ApiParam(example = "admin", value = "账号标识->用户账号/手机号/邮箱号")
            @RequestParam("acc") @NotNull String acc,
            @ApiParam(example = "e10adc3949ba59abbe56e057f20f883e",
                    value = "验证标识->用户密码(MD5密文)/(手机、邮箱)验证码")
            @RequestParam("ver") @NotNull String ver,
            @ApiParam(example = "admin", value = "用户登陆类型前缀")
            @RequestParam("loginPrefix") @NotNull String loginPrefix,
            @ApiParam(example = "manage", value = "客户端来源{manage,ussi}")
            @RequestParam("clientType") @NotNull String clientType
    ) {
        ResultPoJo poJo = ResultPoJo.ok();

        /**
         * 权限校验开始
         * */
        // 1、首先校验用户是否存在,如果不存在则创建
        ResultPoJo<User> user =
                authFocusService.verAccStatus(acc, ver, loginPrefix, clientType);
        if (ConverterUtil.isEmpty(user.getResult())) {
            user.setErrorStatus(ReturnCode.USER_IS_NOT_EXIST);
            return user;
        }
        // 2、接着校验账号标识和验证标识是否正确
        ResultPoJo<Boolean> verAccFlag =
                authFocusService.verAccType(acc, ver);
        if (!verAccFlag.getResult()) {
            return verAccFlag;
        }
        // 3、最后校验客户端入口是否正确
        if (!authFocusService.checkRes(user.getResult(),loginPrefix, clientType)) {
            poJo.setErrorStatus(ReturnCode.NO_SIGN_IN);
            return poJo;
        }

        //最终操作_用户授权
        User resUser = user.getResult();
        AuthUser endUser = authFocusService.authProvide(resUser);
        //返回登录类型
        UserRole userRole = new UserRole().selectOne(new LambdaQueryWrapper<UserRole>()
                .or(ur -> ur.eq(UserRole::getUserId, resUser.getId())));
        Role role = new Role().selectById(userRole.getRoleId());
        endUser.setLoginPrefix(role.getLoginPrefix());

        poJo.setResult(endUser);
        return poJo;
    }

    /**
     * 退出登录
     *
     * @return
     */
    @GetMapping("/logout")
    public ResultPoJo logout() {
        // 退出登录，清空缓存
        try {
            String token = CommonUtil.emptyStr(System.getProperty(HttpHeaders.AUTHORIZATION))
                    .orElse(
                            ((OAuth2AuthenticationDetails) SecurityContextHolder.getContext()
                                    .getAuthentication().getDetails()).getTokenValue()
                    );
            Claims claims = CommonUtil.parseJWT(token);
            Optional.ofNullable(claims).ifPresent(cl -> {
                String token_key = "auth:token:" + cl.getId();
                redisService.remove(token_key);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResultPoJo.ok();
    }

}
