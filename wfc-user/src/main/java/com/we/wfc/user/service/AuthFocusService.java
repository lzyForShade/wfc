package com.we.wfc.user.service;

import cn.hutool.core.lang.Dict;
import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.we.wfc.common.ValidateCodeService;
import com.we.wfc.common.base.BaseException;
import com.we.wfc.common.constants.BaseConstants;
import com.we.wfc.common.enums.DictTypeEnum;
import com.we.wfc.common.enums.ReturnCode;
import com.we.wfc.common.pojo.ResultPoJo;
import com.we.wfc.common.utils.ConverterUtil;
import com.we.wfc.dps.entity.AuthUser;
import com.we.wfc.dps.entity.Role;
import com.we.wfc.dps.entity.User;
import com.we.wfc.dps.entity.UserRole;
import com.we.wfc.dps.service.IRedisService;
import com.we.wfc.user.entity.JWT;
import com.we.wfc.user.entity.enums.RedisKeyEnum;
import com.we.wfc.user.mapper.UserRoleDao;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @Description: 授权处理业务层
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2020/3/14 9:57 下午
 */
@Service
public class AuthFocusService {

    @Autowired
    IRedisService redisService;

    @Autowired
    ValidateCodeService validateCodeService;

    @Autowired
    IUserService iUserService;

    /**
     * port
     */
    @Value("${server.port:8080}")
    private String host;

    @Autowired
    private UserRoleDao userRoleDao;

    /**
     * contextPath
     */
    @Value("${server.servlet.context-path:/wfc}")
    private String contextPath;

    /**
     * 根据用户登录类型前缀来核对客户端入口是否正确
     * 登录系统的底裤,防止客户端篡改请求数据,以至于登录到不该登录的平台
     * 防止用户端的用户登录到管理端，反之则反、
     *
     * @param acc  账号标识
     * @param lpre 用户登录类型前缀
     * @param clis 客户端来源
     * @return boolean
     */
    public boolean checkRes(User acc, String lpre, String clis) {
        String sourceFlag = "0";
        List<Role> roles = new ArrayList<>();

        //查询用户以匹配角色前缀
        User user = new User()
                .selectOne(new LambdaQueryWrapper<User>().eq(User::getLoginName, acc.getLoginName()));
        List<UserRole> userRole = new UserRole().selectAll();
        userRole.stream()
                .filter(x -> x.getUserId().equals(user.getId()))
                .forEach(x -> {
                    if (user.getId().equals(x.getUserId())) {
                        Role role = new Role().selectById(x.getRoleId());
                        roles.add(role);
                    }
                });
        Optional.ofNullable(roles)
                .orElseThrow(() -> new BaseException(ReturnCode.USER_ROLE_IS_NOT_PERFECT));
        /**
         * 其次验证客户端来源和已配置的是否一致
         * */
        for (Role role : roles) {
            //通过登录前缀查询角色信息
            if (!(lpre).equals(role.getLoginPrefix())) {
                continue;
            }
            // 如果两端都可以登录则直接放行
            if (role.getUssi().compareTo(BaseConstants.YES) == 0 &&
                    role.getManage().compareTo(BaseConstants.YES) == 0) {
                return true;
            }
            //如果只可登录一端
            try {
                String fieldName = role.getClass().getDeclaredField(clis).getName();
                String firstLetter = fieldName.substring(0, 1).toUpperCase();
                String getter = "get" + firstLetter + fieldName.substring(1);
                Method method = role.getClass().getMethod(getter, new Class[]{});
                sourceFlag = method.invoke(role, new Object[]{}).toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return sourceFlag.compareTo(BaseConstants.YES) == 0;
    }

    /**
     * 登录账号类型验证处理并且核对验证标识
     *
     * @param accountIdent 账号标识->用户账号、手机号、邮箱
     * @param verifyIdent  验证标识->密码、(手机/邮箱)验证码
     * @return boolean 布尔值
     */
    public ResultPoJo<Boolean> verAccType(String accountIdent, String verifyIdent) {

        ResultPoJo<Boolean> poJo = new ResultPoJo<>();

        //如果账号标识是邮箱/手机号
        if (Validator.isMobile(accountIdent) || Validator.isEmail(accountIdent)) {
            //判断手机号和验证码是否一致
            poJo.setResult(validateCodeService.checkCode(accountIdent, verifyIdent, true));
            poJo.setErrorStatus(ReturnCode.VERIFICATION_EXPIRED_OR_MISMATCH);
        } else {
            //如果账号标识是用户名,则判断密码是否匹配
            User user = new User().selectOne(new LambdaQueryWrapper<User>()
                    .or(n -> n.eq(User::getLoginName, accountIdent)));
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            if (ConverterUtil.isNotEmpty(user)) {
                //如果用户存在则校验密码是否正确
                poJo.setResult(encoder.matches(verifyIdent, user.getPassword()));
                poJo.setErrorStatus(ReturnCode.PASSWORD_IS_INCORRECT);
            }
        }

        return poJo;
    }

    /**
     * 验证账号状态
     *
     * @param acc  账号标识
     * @param lpre 用户登录类型前缀
     * @param clis 客户端来源
     * @return User 用户对象
     */
    @Transactional
    public ResultPoJo<User> verAccStatus(String acc, String ver, String lpre, String clis) {

        ResultPoJo<User> poJo = new ResultPoJo<>();

        //查询用户对象
        User user = new User().selectOne(new LambdaQueryWrapper<User>()
                .or(n -> n.eq(User::getLoginName, acc)));

        //如果用户不存在
        if (ConverterUtil.isEmpty(user) &&
                clis.compareTo(DictTypeEnum.SOURCE_MANAGE.getValue()) == 0) {
            //管理端用户如果不存在则直接返回不存在
            poJo.setErrorStatus(ReturnCode.USER_IS_NOT_EXIST);
        } else if (ConverterUtil.isEmpty(user) &&
                clis.compareTo(DictTypeEnum.SOURCE_USSI.getValue()) == 0) {
            //用户端用户如果不存在则直接注册
            user = iUserService.userRegister(acc, lpre, isUserName(acc) ? ver : null);
        }

        //如果两种情况都不存在,则这个用户的信息异常
        Optional.ofNullable(user).orElseThrow(() -> new BaseException(ReturnCode.USER_IS_NOT_EXIST));

        return poJo.setResult(user);
    }

    /**
     * 授权处理集中提供
     */
    public AuthUser authProvide(User resUser) {

        // 返回对象
        AuthUser authUser = new AuthUser();

        try {
            String rs = HttpRequest.post("http://localhost:" + host +
                    ((ConverterUtil.isNotEmpty(contextPath)) ? contextPath : "") + "/oauth/token")
                    .header("Authorization", "Basic dWFhLXNlcnZpY2U6MTIzNDU2")
                    .form(Dict.create()
                            .set("username", resUser.getLoginName())
                            .set("password", resUser.getRdmPsd())
                            .set("grant_type", "password")
                    ).execute().body();
            JWT token = JSON.parseObject(rs, JWT.class);

            String access_token = token.getAccess_token();
            //Verify that the access_token is empty
            if (ObjectUtil.isNull(access_token)) {
                throw new BaseException("103");

            }
            resUser.set("access_token", "Bearer " + access_token);
            //add redis
            String token_key = StrUtil.format(RedisKeyEnum.AUTH_TOKEN.getKey(), token.getJti());
            redisService.set(token_key, resUser, 60L * 60L);

            // 对象赋值
            ConverterUtil.copyProperties(resUser, authUser);
        } catch (Exception e) {
            if (e instanceof BaseException) {
                throw e;
            } else {
                throw new BaseException("102", e.getMessage());
            }
        }

        return authUser;
    }

    /**
     * 验证账号类型是否为用户名
     */
    public boolean isUserName(String acc) {

        boolean res = false;

        if (Validator.isMobile(acc) || Validator.isEmail(acc)) {
            return res;
        }

        return true;
    }


}
