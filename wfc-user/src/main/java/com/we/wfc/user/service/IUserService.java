package com.we.wfc.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.we.wfc.dps.entity.User;
import com.we.wfc.user.dto.UserSaveDto;

import java.util.function.Function;

/**
 * <p>
 * 用户表  服务类
 * </p>
 *
 * @author zhangby
 * @since 2019-12-03
 */
public interface IUserService extends IService<User> {

    /**
     * 预加载数据
     *
     * @return user
     */
    Function<User, User> preInit();

    /**
     * 保存用户
     *
     * @param userSaveDto u
     */
    void saveUser(UserSaveDto userSaveDto);

    /**
     * 更新用户
     *
     * @param id
     * @param userSaveDto u
     */
    void updateUser(String id, UserSaveDto userSaveDto);

    /**
     * 注册用户(自动归纳角色)
     */
    User userRegister(String account, String lpre, String ver);
}
