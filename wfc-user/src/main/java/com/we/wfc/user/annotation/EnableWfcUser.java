package com.we.wfc.user.annotation;

import com.we.wfc.user.WfcUserImportSelector;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @Description: 用户引入注解
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/11/10 8:27 上午
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import(WfcUserImportSelector.class)
public @interface EnableWfcUser {

}
