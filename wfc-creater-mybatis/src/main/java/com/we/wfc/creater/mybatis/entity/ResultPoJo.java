package com.we.wfc.creater.mybatis.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 返回数据
 *
 * @author zhangby
 * @date 2019-05-13 12:11
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "ResultPoJo", description = "返回数据")
public class ResultPoJo<T> {

    @ApiModelProperty(value = "返回码：000【正确】，其他错误", example = "000")
    private String code;
    @ApiModelProperty(value = "返回信息", example = "成功！")
    private String msg;
    @ApiModelProperty(value = "返回结果", example = "{}")
    private T result;

    public static ResultPoJo ok() {
        return new ResultPoJo()
                .setCode("000")
                .setMsg("成功");
    }

    public static ResultPoJo ok(Object obj) {
        return new ResultPoJo()
                .setCode("000")
                .setMsg("成功")
                .setResult(obj);
    }
}
