package com.we.wfc.creater.mybatis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.we.wfc.creater.mybatis.entity.Creator;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 代码生成 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2020-02-18
 */
@Repository
public interface CreatorDao extends BaseMapper<Creator> {

}