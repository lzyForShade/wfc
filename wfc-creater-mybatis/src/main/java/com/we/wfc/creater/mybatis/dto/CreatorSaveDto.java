package com.we.wfc.creater.mybatis.dto;

import com.we.wfc.creater.mybatis.entity.Creator;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import javax.validation.constraints.NotBlank;

/**
 * 代码生成 保存Dto
 *
 * @author zhangby
 * @date 18/2/20 2:08 pm
 */
@Data
@Accessors(chain = true)
public class CreatorSaveDto {

    @NotBlank(message = "标签名称不能为空")
    @ApiModelProperty(value = "创建名称")
    private String name;

    @NotBlank(message = "数据库表名不能为空")
    @ApiModelProperty(value = "表名")
    private String tableName;

    @ApiModelProperty(value = "创建人")
    private String author;

    @ApiModelProperty(value = "文件输出路径")
    private String outPutDir;

    @NotBlank(message = "包名不能为空")
    @ApiModelProperty(value = "包名")
    private String packageDir;

    @ApiModelProperty(value = "前缀")
    private String tablePrefix;

    @NotBlank(message = "请选择文件生成类型")
    @ApiModelProperty(value = "指定生成类型")
    private String createFile;

    public Creator convert() {
        Creator creator = new Creator();
        BeanUtils.copyProperties(this, creator);
        return creator;
    }
}