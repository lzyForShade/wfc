package com.we.wfc.creater.mybatis.controller;

import cn.hutool.core.lang.Dict;
import com.we.wfc.creater.mybatis.entity.CreatorDataSource;
import com.we.wfc.creater.mybatis.service.IDataSourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * 数据库服务
 *
 * @author zhangby
 * @date 18/2/20 9:34 am
 */
@RestController
@RequestMapping("/api/datasource")
@ApiIgnore
public class DataSourceController {

    @Autowired
    private CreatorDataSource dataSource;

    @Autowired
    IDataSourceService dataSourceService;

    /**
     * 获取数据库连接
     *
     * @return
     */
    @GetMapping("/get")
    public Dict getCreatorDataSource() {
        return Dict.create()
                .set("code", "000")
                .set("msg", "成功")
                .set("result", dataSource);
    }

    /**
     * 获取表名
     *
     * @return
     */
    @GetMapping("/table/get")
    public Dict getTable() {
        try {
            List<Dict> table = dataSourceService.getTable(dataSource);
            return Dict.create()
                    .set("code", "000")
                    .set("msg", "成功")
                    .set("result", table);
        } catch (Exception e) {
            e.printStackTrace();
            return Dict.create()
                    .set("code", "999")
                    .set("msg", "数据库连接异常，请检查配置");
        }
    }
}
