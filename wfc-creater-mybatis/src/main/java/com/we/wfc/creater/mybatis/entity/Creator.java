package com.we.wfc.creater.mybatis.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 代码生成
 * </p>
 *
 * @author zhangby
 * @since 2020-02-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_creator")
@ApiModel(value="Creator对象", description="代码生成")
public class Creator extends Model<Creator> {

private static final long serialVersionUID=1L;

    private String id;

    @ApiModelProperty(value = "创建名称")
    private String name;

    @ApiModelProperty(value = "表名")
    private String tableName;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "创建人")
    private String author;

    @ApiModelProperty(value = "文件输出路径")
    private String outPutDir;

    @ApiModelProperty(value = "包名")
    private String packageDir;

    @ApiModelProperty(value = "前缀")
    private String tablePrefix;

    @ApiModelProperty(value = "指定生成类型")
    private String createFile;

    @ApiModelProperty(value = "创建时间",example = "2020-02-18 00:00:00",hidden = true)
    private Date createDate;

    @ApiModelProperty(value = "更新时间",example = "2020-02-18 00:00:00",hidden = true)
    private Date updateDate;

    @ApiModelProperty(value = "备注信息",hidden = true)
    private String remarks;

    @TableLogic
    @ApiModelProperty(value = "删除标记",example = "0",hidden = true)
    private String delFlag;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}