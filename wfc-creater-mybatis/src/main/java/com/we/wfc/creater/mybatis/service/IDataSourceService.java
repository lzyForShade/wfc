package com.we.wfc.creater.mybatis.service;

import cn.hutool.core.lang.Dict;
import com.we.wfc.creater.mybatis.entity.CreatorDataSource;

import java.util.List;

public interface IDataSourceService {
    List<Dict> getTable(CreatorDataSource dataSource) throws Exception;
}
