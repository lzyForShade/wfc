package com.we.wfc.creater.mybatis.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * data source
 *
 * @author zhangby
 * @date 18/2/20 9:29 am
 */
@Data
@Component
@ConfigurationProperties(prefix = "spring.datasource")
public class CreatorDataSource {
    private String driverClassName;
    private String url;
    private String username;
    private String password;
}
