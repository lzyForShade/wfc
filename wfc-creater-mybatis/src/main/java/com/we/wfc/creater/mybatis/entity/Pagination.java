package com.we.wfc.creater.mybatis.entity;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Optional;

/**
 * 分页参数
 *
 * @author zhangby
 * @date 2019-05-15 14:04
 */
@Data
@Accessors(chain = true)
@AllArgsConstructor
public class Pagination {

    /** 当前页数 */
    @Builder.Default
    private Integer pageNum;
    /** 每页页数 */
    @Builder.Default
    private Integer pageSize;

    /**
     * 获取分页对象
     * @return Page
     */
    public Page page() {
        return new Page(
                Optional.ofNullable(this.pageNum).orElse(1),
                Optional.ofNullable(this.pageSize).orElse(10)
        );
    }
}