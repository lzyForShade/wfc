package com.we.wfc.creater.mybatis.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.we.wfc.creater.mybatis.entity.Creator;

import java.io.FileNotFoundException;

/**
 * <p>
 * 代码生成 服务类
 * </p>
 *
 * @author zhangby
 * @since 2020-02-18
 */
public interface ICreatorService extends IService<Creator> {

    void generator(String id) throws FileNotFoundException;

}
