package com.we.wfc.creater.mybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import springfox.documentation.annotations.ApiIgnore;

/**
 * mybatis-plus 代码生成器
 *
 * @author zhangby
 * @date 9/3/20 6:31 pm
 */
@Controller
@SpringBootApplication(scanBasePackages = "com.we.wfc.creater.mybatis")
@MapperScan("com.we.wfc.creater.mybatis.mapper")
@ApiIgnore
public class WfcCreaterMybatisImportSelector {

    public static void main(String[] args) {
        SpringApplication.run(WfcCreaterMybatisImportSelector.class, args);
    }

    @Value("${we.creator.show:true}")
    private String show;

    @GetMapping("/code/generator")
    public String index() {
        if ("false".equals(show)) {
            return null;
        }
        return "index";
    }
}
