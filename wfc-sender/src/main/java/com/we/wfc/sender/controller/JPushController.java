package com.we.wfc.sender.controller;

import com.we.wfc.sender.service.JPushService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName JPushController
 * @Description
 * @Author licq
 * @Date 2020/3/2 21:02
 */
@Log4j
@Api(tags = "消息推送接口")
@RestController
@RequestMapping(value = "/n/push")
@AllArgsConstructor
public class JPushController {

    private final JPushService jPushService;


    @PostMapping(value = "/push")
    @ApiModelProperty(value = "消息推送")
    public void sendCode() {
        Boolean aBoolean = jPushService.sendToAllIos("1", "2", "3", "4");
        if (aBoolean) {
            log.info("Jpush send success");
        }
    }

}
