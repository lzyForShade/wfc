package com.we.wfc.sender.bo;

import lombok.Getter;
import lombok.Setter;

/**
 * @ClassName EmailCodeVo
 * @Description
 * @Author licq
 * @Date 2020/2/26 23:35
 */
@Getter
@Setter
public class EmailCodeBo {

    /**
     *  邮件验证码
     */
    private String code;

    /**
     * 团队名称
     */
    private String company;

}
