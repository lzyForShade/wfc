package com.we.wfc.sender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.we.wfc.common", "com.we.wfc.sender"})
@EntityScan({"com.we.wfc.common.entity", "com.we.wfc.security.entity"})
public class WfcSenderImportSelector {

    public static void main(String[] args) {
        SpringApplication.run(WfcSenderImportSelector.class, args);
    }

}
