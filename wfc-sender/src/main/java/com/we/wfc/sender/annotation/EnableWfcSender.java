package com.we.wfc.sender.annotation;

import com.we.wfc.sender.WfcSenderImportSelector;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @Description: 开启发送者注解
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2020/2/6 11:56 下午
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Import({ WfcSenderImportSelector.class})
public @interface EnableWfcSender {

}
