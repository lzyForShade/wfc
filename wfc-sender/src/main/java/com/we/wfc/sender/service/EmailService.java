package com.we.wfc.sender.service;

import com.we.wfc.sender.bo.EmailCodeBo;
import com.we.wfc.sender.properties.EmailProp;
import freemarker.template.Template;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName EmailSendService
 * @Description
 * @Author licq
 * @Date 2020/2/25 10:12
 */
@Slf4j
@Service
@AllArgsConstructor
@EnableConfigurationProperties({EmailProp.class})
public class EmailService {

    private final EmailProp emailProp;

    private final JavaMailSender javaMailSender;

    private final FreeMarkerConfigurer configurer;

    /**
     * 发送纯文本邮件
     *
     * @param to      收件人
     * @param subject 邮件主题
     * @param content 邮件内容
     * @return
     */
    public Boolean sendTextMail(String to, String subject, String content) {
        return sendMail(to, subject, content, false, null);
    }

    /**
     * 发送html格式邮件
     *
     * @param to      收件人
     * @param subject 邮件主题
     * @param content 邮件内容
     * @return
     */
    public Boolean sendHtmlMail(String to, String subject, String content) {
        return sendMail(to, subject, content, true, null);
    }

    /**
     * 发送带附件邮件
     *
     * @param to       收件人
     * @param subject  邮件主题
     * @param content  邮件内容
     * @param filePath 文件路径
     * @return
     */
    public Boolean sendFileMail(String to, String subject, String content, String filePath) {
        return sendMail(to, subject, content, true, filePath);
    }

    /**
     * 发送html+附件邮件
     *
     * @param to       收件人
     * @param subject  邮件主题
     * @param content  邮件内容
     * @param filePath 文件路径
     * @return
     */
    public Boolean sendHtmlFileMail(String to, String subject, String content, String filePath) {
        return sendMail(to, subject, content, true, filePath);
    }

    /**
     * 批量发送多邮件
     *
     * @param to       多个收件人
     * @param subject  邮件主题
     * @param content  邮件内容
     * @param filePath 多个附件：无附件为null
     * @return
     */
    public Boolean sendManyMail(String[] to, String subject, String content, Boolean type, String[] filePath) {
        return sendToManyMail(to, subject, content, type, filePath);
    }

    /**
     * 发送邮件
     *
     * @param to       收件人
     * @param subject  邮件主题
     * @param content  邮件内容
     * @param type     邮件类型（true：纯文本 false：html）
     * @param filePath 附带文件
     * @return
     */
    public Boolean sendMail(String to, String subject, String content, Boolean type, String filePath) {
        //创建邮件对象
        MimeMessage mMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mMessageHelper;
        try {
            mMessageHelper = new MimeMessageHelper(mMessage, true);
            //发件人
            mMessageHelper.setFrom(emailProp.from);
            //收件人
            mMessageHelper.setTo(to);
            //邮件主题
            mMessageHelper.setSubject(subject);
            //发送时间
            mMessageHelper.setSentDate(new Date());
            // 邮件的文本内容，true表示文本以html格式打开
            if (type) {
                //html模板格式处理
                Map<String, Object> map = new HashMap<>();
                EmailCodeBo params = new EmailCodeBo();
                params.setCode(content);
                params.setCompany(emailProp.getCompany());
                //存入code
                map.put("params", params);
                //获取ftl模板
                Template template = configurer.getConfiguration().getTemplate("/mailt.ftl");
                //生成html格式模板内容
                String html = FreeMarkerTemplateUtils.processTemplateIntoString(template, map);
                mMessageHelper.setText(html, true);
            } else {
                mMessageHelper.setText(content, false);
            }
            //附带文件
            if (StringUtils.isNotEmpty(filePath)) {
                //获取文件
                File file = new File(filePath);
                //文件名
                String fileName = file.getName();
                //文件
                FileSystemResource resource = new FileSystemResource(file);
                //在邮件中添加邮件
                mMessageHelper.addAttachment(fileName, resource);
            }
            //发送邮件
            javaMailSender.send(mMessage);
            //日志信息
            log.info("邮件已经发送。");
        } catch (Exception e) {
            log.error("发送邮件时发生异常！", e);
        }
        return true;
    }

    /**
     * 批量发送邮件
     *
     * @param to       收件人
     * @param subject  邮件主题
     * @param content  邮件内容
     * @param type     邮件类型（true：纯文本 false：html）
     * @param filePath 附带文件
     * @return
     */
    public Boolean sendToManyMail(String[] to, String subject, String content, Boolean type, String[] filePath) {
        //创建邮件对象
        MimeMessage mMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mMessageHelper;
        try {
            mMessageHelper = new MimeMessageHelper(mMessage, true);
            //发件人
            mMessageHelper.setFrom(emailProp.from);
            //多个收件人
            mMessageHelper.setTo(to);
            //邮件主题
            mMessageHelper.setSubject(subject);
            //发送时间
            mMessageHelper.setSentDate(new Date());

            //内容和附件传输格式
            Multipart multipart = new MimeMultipart();
            MimeBodyPart mBodyPart1 = new MimeBodyPart();

            // 邮件的文本内容，true表示文本以html格式打开
            if (type) {
                //html模板格式处理
                Map<String, Object> map = new HashMap<>();
                EmailCodeBo params = new EmailCodeBo();
                params.setCode(content);
                params.setCompany(emailProp.getCompany());
                //存入code
                map.put("params", params);
                //获取ftl模板
                Template template = configurer.getConfiguration().getTemplate("/mailt.ftl");
                //生成html格式模板内容
                String html = FreeMarkerTemplateUtils.processTemplateIntoString(template, map);
                mBodyPart1.setContent(html, "text/html; charset=utf-8");
            } else {
                mBodyPart1.setText(content);
            }
            multipart.addBodyPart(mBodyPart1);

            //附带文件
            if (filePath != null || filePath.length != 0) {
                for (int i = 0; i < filePath.length; i++) {
                    MimeBodyPart mBodyPart2 = new MimeBodyPart();
                    String file = filePath[i].split(",")[0];
                    //获取文件数据源
                    FileDataSource fileData = new FileDataSource(file);
                    //文件放入bodypart
                    mBodyPart2.setDataHandler(new DataHandler(fileData));
                    //文件名放入bodypart
                    mBodyPart2.setFileName(fileData.getName());
                    multipart.addBodyPart(mBodyPart2);
                }
            }
            mMessage.setContent(multipart);
            //发送邮件
            javaMailSender.send(mMessage);
            //日志信息
            log.info("邮件已经发送。");
        } catch (Exception e) {
            log.error("发送邮件时发生异常！", e);
        }
        return true;
    }
}
