package com.we.wfc.sender.controller;

import com.we.wfc.common.ValidateCodeService;
import com.we.wfc.common.enums.ReturnCode;
import com.we.wfc.common.pojo.ResultPoJo;
import com.we.wfc.sender.service.CmuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @Description: 通信发送接口
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/12/12 11:45 上午
 */
@Api(tags = "电信服务接口")
@RestController
@RequestMapping(value = "/n/cmu")
@AllArgsConstructor
public class CmuController {

    private final CmuService cmuService;

    private final ValidateCodeService validateCodeService;

    @PostMapping(value = "/cvCode")
    @ApiModelProperty(value = "发送验证码", notes = "主要用来给手机号/邮箱发送验证码")
    public ResultPoJo<ReturnCode> sendCode(
            @ApiParam(name = "account", value = "通信账号", example = "xxx@xxx.com/17777786352")
            @RequestParam String account) {
        return cmuService.sendCode(account);
    }

    @GetMapping(value = "/validCode")
    @ApiModelProperty(value = "核验验证码", notes = "主要用来核验验证码是否匹配")
    public ResultPoJo<ReturnCode> validCode(
            @ApiParam(name = "account", value = "通信账号", example = "xxx@xxx.com/17777786352")
            @RequestParam String account,
            @ApiParam(name = "code", value = "验证码", example = "1234")
            @RequestParam String code) {

        ResultPoJo<ReturnCode> poJo = new ResultPoJo<>();

        if (!validateCodeService.checkSMSCode(account, code, true)) {
            poJo.setResult(ReturnCode.VERIFICATION_EXPIRED_OR_MISMATCH);
        } else {
            poJo.setResult(ReturnCode.VERIFICATION_CODE_MATCHES_THE_PHONE);
        }

        return poJo;
    }
}
