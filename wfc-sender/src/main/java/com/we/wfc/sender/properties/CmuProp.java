package com.we.wfc.sender.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Description:
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/10/31 4:08 下午
 */
@Setter
@Getter
@ConfigurationProperties(prefix = "we.cmu")
public class CmuProp {

    /**
     * 是否为测试发送
     * */
    private boolean cmuTest;

    /**
     * 过期时间(秒)
     * */
    private int exprire;
}
