package com.we.wfc.sender.service;

import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.PushPayload;
import com.we.wfc.common.utils.ConverterUtil;
import com.we.wfc.sender.config.JPushConfig;
import com.we.wfc.sender.properties.SmsProp;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName JPushService
 * @Description 极光消息推送业务
 * @Author licq
 * @Date 2020/3/2 17:23
 */
@Slf4j
@Service
@AllArgsConstructor
@EnableConfigurationProperties({SmsProp.class})
public class JPushService {

    private final SmsProp smsProp;

    private final JPushConfig jPushConfig;

    private JPushClient jPushClient;

    public void clientInit() {
        if (ConverterUtil.isNotEmpty(jPushClient)) {
            jPushClient = new JPushClient(smsProp.getMasterSecret(), smsProp.getAppkey());
        }
    }

    /**
     * 推送给设备标识参数的用户
     *
     * @param aliasList          别名或别名组
     * @param notification_title 通知内容标题
     * @param msg_title          消息内容标题
     * @param msg_content        消息内容
     * @param extrasparam        扩展字段
     * @return result              false推送失败， true推送成功
     */
    public Boolean sendToAliasList(List<String> aliasList, String notification_title, String msg_title, String msg_content, String extrasparam) {
        clientInit();
        boolean result = false;
        try {
            PushPayload pushPayload = jPushConfig.buildPushObject_all_aliasList_alertWithTitle(aliasList, notification_title, msg_title, msg_content, extrasparam);
            log.info("推送给设备标识参数的用户： " + pushPayload);
            PushResult pushResult = jPushClient.sendPush(pushPayload);
            log.info("推送结果： " + pushResult);
            if (pushResult.getResponseCode() == 200) {
                result = true;
            }
        } catch (APIConnectionException e) {
            e.printStackTrace();
        } catch (APIRequestException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 推送给Tag参数的用户
     *
     * @param tagsList           Tag或Tag组
     * @param notification_title 通知内容标题
     * @param msg_title          消息内容标题
     * @param msg_content        消息内容
     * @param extrasparam        扩展字段
     * @return result              false推送失败，true推送成功
     */
    public Boolean sendToTagList(List<String> tagsList, String notification_title, String msg_title, String msg_content, String extrasparam) {
        clientInit();
        boolean result = false;
        try {
            PushPayload pushPayload = jPushConfig.buildPushObject_all_tagList_alertWithTitle(tagsList, notification_title, msg_title, msg_content, extrasparam);
            log.info("推送给Tag参数的用户：" + pushPayload);
            PushResult pushResult = jPushClient.sendPush(pushPayload);
            log.info("推送结果： " + pushResult);
            if (pushResult.getResponseCode() == 200) {
                result = true;
            }
        } catch (APIConnectionException e) {
            e.printStackTrace();
        } catch (APIRequestException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 发送给所有安卓用户
     *
     * @param notification_title 通知内容标题
     * @param msg_title          消息内容标题
     * @param msg_content        消息内容
     * @param extrasparam        扩展字段
     * @return result             false推送失败，true推送成功
     */
    public Boolean sendToAllAndroid(String notification_title, String msg_title, String msg_content, String extrasparam) {
        clientInit();
        boolean result = false;
        try {
            PushPayload pushPayload = jPushConfig.buildPushObject_android_all_alertWithTitle(notification_title, msg_title, msg_content, extrasparam);
            log.info("发送给所有安卓用户： " + pushPayload);
            PushResult pushResult = jPushClient.sendPush(pushPayload);
            log.info("推送结果： " + pushResult);
            if (pushResult.getResponseCode() == 200) {
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 发送给所有IOS用户
     *
     * @param notification_title 通知内容标题
     * @param msg_title          消息内容标题
     * @param msg_content        消息内容
     * @param extrasparam        扩展字段
     * @return result         false推送失败，1推送成功
     */
    public Boolean sendToAllIos(String notification_title, String msg_title, String msg_content, String extrasparam) {
        clientInit();
        boolean result = false;
        try {
            PushPayload pushPayload = jPushConfig.buildPushObject_ios_all_alertWithTitle(notification_title, msg_title, msg_content, extrasparam);
            log.info("发送给所有IOS用户： " + pushPayload);
            PushResult pushResult = jPushClient.sendPush(pushPayload);
            log.info("推送结果： " + pushResult);
            if (pushResult.getResponseCode() == 200) {
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 发送给所有用户
     *
     * @param notification_title 通知内容标题
     * @param msg_title          消息内容标题
     * @param msg_content        消息内容
     * @param extrasparam        扩展字段
     * @return result             0推送失败，1推送成功
     */
    public Boolean sendToAll(String notification_title, String msg_title, String msg_content, String extrasparam) {
        clientInit();
        boolean result = false;
        try {
            PushPayload pushPayload = jPushConfig.buildPushObject_android_and_ios(notification_title, msg_title, msg_content, extrasparam);
            log.info("发送给所有用户： " + pushPayload);
            PushResult pushResult = jPushClient.sendPush(pushPayload);
            log.info("推送结果" + pushResult);
            if (pushResult.getResponseCode() == 200) {
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
