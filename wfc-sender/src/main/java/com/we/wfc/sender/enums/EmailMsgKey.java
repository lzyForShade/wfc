package com.we.wfc.sender.enums;

import com.we.wfc.common.base.BaseEnum;

/**
 * @Description: 邮件发送短信枚举
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2020/1/19 8:23 下午
 */
public enum EmailMsgKey implements BaseEnum {

    EMAIL_KEY("尊敬的客户您好。您本次的验证码为:", "0"),
    ;

    private String label;
    private String value;

    @Override
    public String getLabel() {
        return this.label;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    EmailMsgKey(String label, String value) {
        this.label = label;
        this.value = value;
    }
}
