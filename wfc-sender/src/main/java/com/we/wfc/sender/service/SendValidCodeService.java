package com.we.wfc.sender.service;

import cn.hutool.core.lang.Validator;
import com.we.wfc.common.cache.CacheKey;
import com.we.wfc.common.cache.JedisDao;
import com.we.wfc.common.constants.BaseConstants;
import com.we.wfc.common.utils.ConverterUtil;
import com.we.wfc.sender.po.UnifiedSmsPo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @Description: 邮箱/短信验证码业务
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2020/2/24 6:07 下午
 */
@Slf4j
@Service
@AllArgsConstructor
public class SendValidCodeService {

    private final SmsService smsService;

    private final EmailService emailService;

    private final JedisDao jedis;

    /**
     * 发送验证码
     *
     * @param account 手机号/邮箱
     * @param length  验证码长度
     * @param seconds 过期时间
     * @return
     */
    public Boolean sendValidCode(String account, String code, int length, int seconds) {

        //RedisKey
        String cmuKey = null;
        boolean res = false;
        Boolean sendFlag = false;

        //如果为空就生成随机数
        if (code == null) {
            code = ConverterUtil.getCheckCode(ConverterUtil.RANDOM_TYPE_NUM, length);
        }
        //判断账户类型是否为手机
        if (Validator.isMobile(account)) {
            //发送手机短信业务逻辑
            cmuKey = CacheKey.getPN(account);
            UnifiedSmsPo unifiedSmsPo = new UnifiedSmsPo(177410, account, "code", code);
            sendFlag = smsService.sendSms(unifiedSmsPo);
        } else {
            //发送邮件业务逻辑
            emailService.sendHtmlMail(account, BaseConstants.TITLE_TEXT_VAILDCODE, code);
            cmuKey = CacheKey.getEm(account);
        }

        //如果发送成功就插入到缓存中并设置过期时间
        if (sendFlag) {
            log.info("Account: " + account + " sent" + "msg is " + code);
            //Redis入参
            res = jedis.setSerializeExpire(cmuKey, code, seconds);
        }

        return res;
    }
}
