package com.we.wfc.creater.annotation;


import com.we.wfc.creater.WfcCreaterImportSelector;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @Description: creater引入注解
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/11/10 8:27 上午
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import(WfcCreaterImportSelector.class)
public @interface EnableWfcCreater {
}
