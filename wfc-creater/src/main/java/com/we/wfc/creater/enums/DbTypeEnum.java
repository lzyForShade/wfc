package com.we.wfc.creater.enums;

import com.baomidou.mybatisplus.generator.config.rules.IColumnType;

/**
 * @Description: 数据库常用字段的常量类
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2020/1/15 1:58 下午
 */
public enum DbTypeEnum implements IColumnType {
    // java8 新时间类型
    OFFSET_DATE_TIME("OffsetDateTime", "java.time.OffsetDateTime");

    /**
     * 类型
     */
    private final String type;

    /**
     * 包路径
     */
    private final String pkg;

    DbTypeEnum(final String type, final String pkg) {
        this.type = type;
        this.pkg = pkg;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getPkg() {
        return pkg;
    }
}
